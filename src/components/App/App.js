import React from 'react';
import './App.scss';
import Gallery from '../Gallery';

class App extends React.Component {
  static propTypes = {
  };

  constructor() {
    super();
    this.state = {
      tag: 'art',
      prevTag: 'art',
      titleFilter: ''
    };
  }

  render() {
    return (
      <div className="app-root">
        <div className="app-header">
          <h2>Flickr Gallery</h2>
          <h4 className="search-box">Search:</h4>
          <input className="app-input" onChange={event => this.setState({tag: event.target.value,
          titleFilter:''})}
           value={this.state.tag}/>
          <h4 className="filter-box">Filter (By Title):</h4>
          <input className="app-input" onChange={event => this.setState({titleFilter: event.target.value,
            prevTag: this.state.tag})}
            value={this.state.titleFilter}/>

        </div>
        <Gallery tag={this.state.tag} prevTag={this.state.prevTag} titleFilter={this.state.titleFilter}/>
      </div>
    );
  }
}

export default App;
