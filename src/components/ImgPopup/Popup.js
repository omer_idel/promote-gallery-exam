import React from 'react';
import '../ImgPopup/Popup.scss';
import FontAwesome from 'react-fontawesome';


class Popup extends React.Component {

    render() {
        return (
            <div className="popup-parent">
                <div className="popup-image">
                    <FontAwesome className="image-closing-button" name="f457 fas fa-times-circle" title="window-close" onClick={this.props.closePopup}/>
                    <img className={'img-popup-'+this.props.filter} src={this.props.popUpImg}/>
                </div>
            </div>
        );
    }
}

export default Popup;
