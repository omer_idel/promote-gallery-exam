import React from 'react';
import PropTypes from 'prop-types';
import FontAwesome from 'react-fontawesome';
import './Image.scss';
import Popup from '../ImgPopup/Popup.js';
import fileDownload from 'js-file-download';
import axios from 'axios';

class Image extends React.Component {
  static propTypes = {
    dto: PropTypes.object,
    galleryWidth: PropTypes.number,
    imageSize: PropTypes.number
    
  };

  constructor(props) {
    super(props);
    this.state = {
    
      isExpand: false,
      popUpImg: '',
      filter: 'none',
      filterToPopup: 'none'
    };
  }

  urlFromDto(dto) {
    return `https://farm${dto.farm}.staticflickr.com/${dto.server}/${dto.id}_${dto.secret}.jpg`;
  }

  clickExpandPopup = () => {
    this.setState({
      isExpand: !this.state.isExpand,
      popUpImg: this.urlFromDto(this.props.dto)
    })
    if(this.state.filter !== 'none'){
      this.setState({
        filter: 'none'
      })
    }
    if(this.state.filter === 'none'){
      this.setState({ filter: this.state.filterToPopup})
    }
  }

   clickFilter = () => {
    let filters = ['blur', 'invert', 'opacity', 'saturate', 'none'];
    const theFilter = filters[Math.floor(Math.random()*filters.length)];
    this.setState({
      filter: theFilter,
      filterToPopup: theFilter
    })

  }

  handleClone() {
    this.props.handleClone(this.props.dto);
    }

   handleDelete() {
     this.props.handleDelete(this.props.dto);
   }

   clickDownload = (url, filename) => {

    axios.get(url, {
      responseType: 'blob'
    })
    .then((res) => {
      fileDownload(res.data, filename)
    })

  }

  render() {
    return (

      <div
        className={'image-root-'+this.state.filter}

        style={{
          backgroundImage: `url(${this.urlFromDto(this.props.dto)})`,
          width: this.props.imageSize + 'px',
          height: this.props.imageSize + 'px'
        }}
        >

        <div className="btn-img">
          <FontAwesome className="image-icon" name="clone" title="clone" onClick={()=> this.handleClone()}/>
          <FontAwesome className="image-icon" name="filter" title="filter" onClick={this.clickFilter}/>
          <FontAwesome className="image-icon" name="expand" title="expand" onClick={this.clickExpandPopup}/>
          <FontAwesome className="image-icon" name="far fa-trash-alt" title="trash" onClick={()=> this.handleDelete()}/>
          <FontAwesome className="image-icon" name="cloud-download-alt" title="download" onClick={() => this.clickDownload(this.urlFromDto(this.props.dto),this.props.dto.id+'.jpg')}/>

          {this.state.isExpand ? (
            <Popup
            popUpImg={this.state.popUpImg}
            closePopup={this.clickExpandPopup}
            filter={this.state.filterToPopup}
            />
            ) : null
          }

        </div>
      </div>
    );
  }
}

export default Image;
