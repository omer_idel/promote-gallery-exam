import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import Image from '../Image';
import './Gallery.scss';

const WAIT_TIME = 500;
const extraInitialMarginOfWindowSize = 17;
const imagesToAdd = 35;

class Gallery extends React.Component {
  static propTypes = {
    tag: PropTypes.string,
    prevTag: PropTypes.string,
    titleFilter: PropTypes.string
  };

  constructor(props) {
    super(props);
    const width = document.body.clientWidth - extraInitialMarginOfWindowSize;
    const imagesPerRow = Math.ceil(width / 200);
    const size = width / imagesPerRow

    this.state = {
      titleFilter: this.props.titleFilter,
      allImages: [],
      images: [],
      length: 0,
      timeout: 0,
      galleryWidth: width,
      imageSize: size,
      hasMore: true
    };
  }

  getGalleryWidth(){
    try {
      return document.body.clientWidth;
    } catch (e) {
      return 1000;
    }
  }

  getImages(tag) {

    const getImagesUrl = `services/rest/?method=flickr.photos.search&api_key=522c1f9009ca3609bcbaf08545f067ad&tags=${tag}&tag_mode=any&per_page=1000&format=json&nojsoncallback=1`;
    const baseUrl = 'https://api.flickr.com/';
    axios({
      url: getImagesUrl,
      baseURL: baseUrl,
      method: 'GET'
    })
      .then(res => res.data)
      .then(res => {
        if (
          res &&
          res.photos &&
          res.photos.photo &&
          res.photos.photo.length > 0
        ) {
          this.setState({
            allImages: res.photos.photo,
            images: res.photos.photo.slice(0, imagesToAdd),
            length: imagesToAdd,
            hasMore: true
          });
        }
      });
  }

  LoadMoreData() {
    const currentLenght = this.state.images.length;
    const newLength = currentLenght + imagesToAdd;
    if (newLength < this.state.allImages.length) {
      this.setState({
        images: this.state.images.concat(this.state.allImages.slice(currentLenght , newLength)),
        length: newLength
      })
    }
    else if (this.state.allImages.length - currentLenght > 0) {
      this.setState({
        images: this.state.images.concat(this.state.allImages.slice(currentLenght , this.state.allImages.length)),
        length: this.state.allImages.length
      })
    }
    else {
      this.setState({
        hasMore: false
      })
    }
  }
  
  componentDidMount() {
 
    const self = this;
    window.addEventListener('resize', function (){
      const x = self.getGalleryWidth()
      const imagesPerRow = Math.ceil(self.state.galleryWidth / 200);
      self.setState({
        galleryWidth: x,
        imageSize: (x / imagesPerRow)
      });
    });
    this.getImages(this.props.tag);

    window.addEventListener('scroll',this.trackScrolling);
    
  }

  trackScrolling = () => {
    if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
      this.LoadMoreData();
      document.removeEventListener('scroll', this.trackScrolling);
    }
  };

  componentWillReceiveProps(props) {

    if(props.prevTag !== props.tag){
    var searchText = props.tag;
    if(this.timeout){
      clearTimeout(this.timeout);
    }
    this.timeout = setTimeout(() => {
      this.getImages(searchText);
    }, WAIT_TIME);
  }
  else{
      let arrCopy = this.state.allImages;
      let arr = [];
      for (let index = 0; index < this.state.length; index++) {
        if(arrCopy[index].title.toLowerCase().includes(props.titleFilter.toLowerCase())){
          arr.push(arrCopy[index]);
        }
      }
      this.setState({
        images: arr
      })
    }
  }

  handleClone = image => {
    let imagesCopy = [...this.state.images];
    imagesCopy.push(image);
    this.setState({ images: imagesCopy });
  };

  handleDelete = image => {
    const imgCopyWithoutImgToRemove = this.state.images.filter(function(obj){
      return obj.id !== image.id;
    });
    const imgCopyWithoutImgToRemoveAll = this.state.allImages.filter(function(obj){
      return obj.id !== image.id;
    });
    this.setState({
      images: imgCopyWithoutImgToRemove,
      allImages: imgCopyWithoutImgToRemoveAll
    })

  };

  render() {
    return (
      <div className="gallery-root">
        {this.state.images.map((dto, index) => {
          return <Image key={'image-' + index} dto={dto} galleryWidth={this.state.galleryWidth}
          handleClone={image => this.handleClone(image)} handleDelete={image => this.handleDelete(image)}
          galleryWidth={this.state.galleryWidth} imageSize={this.state.imageSize}
          />;
        })}
        {!this.state.hasMore ? (
            <h3>You're all caught up! Yayyy</h3>
            ) : null
          }
      </div>
    );
  }
}

export default Gallery;
